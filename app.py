from flask import Flask
# 引用数据库启动文件
from mapper.exts import db
# 引用数据库配置文件
import config
# 引用数据库
from controller.user_controller import *

app = Flask(__name__)
app.config.from_object(config)
db.init_app(app)

app.register_blueprint(user, url_prefix="/user")

if __name__ == '__main__':
    app.run(debug=True)
