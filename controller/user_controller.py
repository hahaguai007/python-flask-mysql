from flask import Blueprint, jsonify, request, json
from model.user_model import User
from mapper.exts import db

user = Blueprint('user', __name__)


# 查询全部 http://127.0.0.1:5000/user/list
@user.route('/list')
def list_users():
    users = User.query.all()
    print(users)
    users_output = []
    for user in users:
        users_output.append(user.to_json())
    return jsonify(users_output)


# 条件查询 http://127.0.0.1:5000/user/findUser/2
@user.route('/findUser/<username>')
def search_user(username):
    """
    print("接收参数：", username)
    # 根据用户名查询符合条件的第一条数据
    user1 = User.query.filter_by(username=username).first()
    print("查询1：", user1.to_json())
    # 根据用户名查询
    user2 = User.query.filter_by(username=username)
    print("查询2：", user1.to_json())
    # 匹配用户名开头为1的用户
    users = User.query.filter(User.username.startswith('1')).all()
    print(users)
    users_output = []
    for user3 in users:
        users_output.append(user3.to_json())
    print("查询3：", users_output)
    # 排序
    user_list = User.query.order_by("username")
    print(user_list)
    users_output_list = []
    for user4 in user_list:
        users_output_list.append(user4.to_json())
    print("查询4：", users_output_list)
    # 查询TOP3
    user_list1 = User.query.limit(3).all()
    print(user_list1)
    users_output_list1 = []
    for user5 in user_list1:
        users_output_list1.append(user5.to_json())
    print("查询5：", users_output_list1)
    """
    # 获取第一条记录
    user6 = User.query.first()
    print("查询6：", user6.to_json())
    return jsonify(user6.to_json())


# 查询用户详情 http://127.0.0.1:5000/user/details/2
@user.route('/details/<userid>')
def find_user(userid):
    user = User.query.get(userid)
    return jsonify(user.to_json())


# 分页 http://127.0.0.1:5000/user/loadUserPage/2/1
@user.route('/loadUserPage/<int:page>/<int:per_page>')
def list_user(page, per_page):
    """ 用户分页 """
    # 每一页的数据大小-per_page 页码-page
    # 1. 查询用户信息
    user = User.query
    # 2. 准备分页的数据
    print(page, per_page)
    user_page_data = user.paginate(page, per_page=per_page)
    users_output = []
    for user in user_page_data.items:
        users_output.append(user.to_json())
    print("当前页的数据列表", users_output)
    print("是否有上一页", user_page_data.has_prev)
    print("是否下一页", user_page_data.has_next)
    print("上一页的页码", user_page_data.prev_num)
    print("下一页的页码", user_page_data.next_num)
    print("总记录数", user_page_data.total)
    print("总页数", user_page_data.pages)
    return jsonify(users_output)


# 添加 http://127.0.0.1:5000/user/add/2/1
@user.route('/add/<username>/<pwd>')
def add_user(username, pwd):
    print(username, pwd)
    userinfo = User(username=username, pwd=pwd)
    db.session.add(userinfo)
    db.session.commit()
    # # 接受前端发来的数据
    # data = json.loads(request.form.get('data'))
    #
    # # lesson: "Operation System"
    # # score: 100
    # lesson = data["lesson"]
    # score = data["score"]
    #
    # # 自己在本地组装成Json格式,用到了flask的jsonify方法
    # info = dict()
    # info['name'] = "pengshuang"
    # info['lesson'] = lesson
    # info['score'] = score
    # return jsonify(info)
    return jsonify("ADD_SUCCESS")


# 删除 http://127.0.0.1:5000/user/delete/3
@user.route('/delete/<int:id>')
def delete_user(id):
    print(id)
    # 第一种
    user1 = User.query.filter().first()
    print(user1.id)
    db.session.delete(user1)
    db.session.commit()
    # 第二种
    user2 = User.query.filter(User.id == 2).delete()
    db.session.commit()
    return jsonify("DELETE_SUCCESS")


# 修改  http://127.0.0.1:5000/user/update/3
@user.route('/update/<int:id>')
def update_user(id):
    user1 = User.query.filter(User.id == id).first()
    user1.username = "我是修改后的"
    db.session.merge(user1)
    db.session.commit()
    return jsonify("UPDATE_SUCCESS")