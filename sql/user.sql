/*
 Navicat Premium Data Transfer

 Source Server         : project
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : tdata

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 07/04/2021 16:33:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nick` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '12', '123', '123321', 'tiantian.com');
INSERT INTO `user` VALUES (2, 'yang', '312', '123321', 'tiantian.com');
INSERT INTO `user` VALUES (3, '我是修改后的', '19', '123321', 'tiantian.com');
INSERT INTO `user` VALUES (4, 'li', '22', '123321', 'tiantian.com');
INSERT INTO `user` VALUES (5, '123', '22', '123321', 'tiantian.com');
INSERT INTO `user` VALUES (6, '112', '22', '123321', 'tiantian.com');
INSERT INTO `user` VALUES (7, '老王', '222', '123321', 'tiantian.com');

SET FOREIGN_KEY_CHECKS = 1;
